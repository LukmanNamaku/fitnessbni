package com.id.fitnessBni.repository;

import com.id.fitnessBni.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;


/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Optional<Customer> findByEmail(String email);
    Customer findByAccountId(int accountId);
    Customer findTopByEmail(String email);
    Customer findByPhoneNumber(String username);
    Customer findByEmailAndVerificationCode(String email, String verificationCode);

}
