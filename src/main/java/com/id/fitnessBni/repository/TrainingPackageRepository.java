package com.id.fitnessBni.repository;

import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.entity.TrainingPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Repository
public interface TrainingPackageRepository extends JpaRepository<TrainingPackage, Integer> {

//    Optional<Customer> findByCustomerAndTrainingPackage(Customer customer, TrainingPackage trainingPackage);
    TrainingPackage findById(int id);

}
