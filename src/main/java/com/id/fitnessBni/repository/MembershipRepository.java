package com.id.fitnessBni.repository;

import com.id.fitnessBni.entity.MemberPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 08/11/2023
 */

@Repository
public interface MembershipRepository extends JpaRepository<MemberPackage, Integer>{

}
