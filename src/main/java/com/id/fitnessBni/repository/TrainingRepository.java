package com.id.fitnessBni.repository;

import com.id.fitnessBni.entity.Training;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */


public interface TrainingRepository  extends JpaRepository<Training, Integer> {

    Training findById(int id);

}