package com.id.fitnessBni.repository;

import com.id.fitnessBni.dto.creditcard.CreditCardDTO;
import com.id.fitnessBni.entity.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Repository
public interface CreditCardRepositoy  extends JpaRepository<CreditCard, Integer> {

    CreditCard findById(int id);

}
