package com.id.fitnessBni.repository;

import com.id.fitnessBni.entity.MembershipStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Repository
public interface MembershipStatusRepository extends JpaRepository<MembershipStatus, Integer> {

    MembershipStatus findById(int id);
    MembershipStatus findByStatusMemberCode(String memberCode);
    MembershipStatus deleteById(int id);

}
