package com.id.fitnessBni.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 05/11/2023
 */

@Data
@Entity
@Setter
@Getter
public class Customer {

    private static final long OTP_VALID_DURATION = 5 * 60 * 1000;
    // 5 minutes

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountId;

    private String name;
    private String email;
    private String password;
    private String phoneNumber;
    private boolean isVerified;

    private String verificationCode;
    private Date verificationCodeRequestedTime;

    private String resetToken;

    @ManyToOne
    @JoinColumn(name = "statusCode")
    private PaymentStatus paymentStatus;
    private String paymentOTP;
    private LocalDateTime paymentOTPExpiration;

    private BigDecimal billAmount;
    private boolean isBillVerified;

    @ManyToOne
    @JoinColumn(name = "creditCardId")
    private CreditCard creditCardInfo;

    @ManyToOne
    @JoinColumn(name = "statusMemberCode")
    private MembershipStatus membershipStatus;

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getResetToken() {
        return resetToken;
    }

}
