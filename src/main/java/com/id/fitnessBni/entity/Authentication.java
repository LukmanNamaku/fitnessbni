package com.id.fitnessBni.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 05/11/2023
 */

@Data
@Entity
@Setter
@Getter
public class Authentication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String token;

    private LocalDateTime expirationDateTime;

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Customer customer;

    public Authentication(String token, LocalDateTime expirationDateTime, Customer customer) {

    }

    public void updateExpiration() {
        this.expirationDateTime = LocalDateTime.now().plusHours(5);
    }

}
