package com.id.fitnessBni.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 05/11/2023
 */

@Data
@Entity
@Setter
@Getter
public class TrainingPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    private String name;
    private double pricePerSession;
    private String schedule;
    private int totalSessions;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trainingPackage")
    private List<Training> training;

}
