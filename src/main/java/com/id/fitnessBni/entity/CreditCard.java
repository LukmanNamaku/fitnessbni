package com.id.fitnessBni.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 05/11/2023
 */

@Data
@Entity
@Setter
@Getter
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int creditCardId;

    private String cardName;
    private String cardNumber;
    private String ccvCode;
    private Date expirationDate;
}
