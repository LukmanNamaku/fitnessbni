package com.id.fitnessBni.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 05/11/2023
 */

@Data
@Entity
@Setter
@Getter
public class MemberPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private int remainingSessions;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private TrainingPackage trainingPackage;
}
