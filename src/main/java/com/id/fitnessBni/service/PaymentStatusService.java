package com.id.fitnessBni.service;

import com.id.fitnessBni.dto.paymentstatus.PaymentStatusCreateRequest;
import com.id.fitnessBni.dto.paymentstatus.PaymentStatusUpdateRequest;
import com.id.fitnessBni.entity.PaymentStatus;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */


public interface PaymentStatusService {

    PaymentStatus findById(int id);
    List<PaymentStatus> findAll();
    PaymentStatus save(PaymentStatusCreateRequest paymentStatusCreateRequest);
    PaymentStatus update(PaymentStatusUpdateRequest paymentStatusUpdateRequest);
    PaymentStatus delete(int id);

}
