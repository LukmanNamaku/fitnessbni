package com.id.fitnessBni.service;

import com.id.fitnessBni.dto.creditcard.CreditCardUpdateRequest;
import com.id.fitnessBni.dto.customer.CustomerCreateRequest;
import com.id.fitnessBni.dto.customer.CustomerUpdateRequest;
import com.id.fitnessBni.dto.customer.ForgotPassword;
import com.id.fitnessBni.entity.CreditCard;
import com.id.fitnessBni.entity.Customer;
import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

public interface CustomerService {

    Customer save(CustomerCreateRequest customerCreateRequest);
    List<Customer> findAll();
    void delete(int id);
    Customer findOne(String username);
    Customer findById(int id);
    Customer update(CustomerUpdateRequest customerUpdateRequest);

    Customer addCustomerCreditCard (CreditCardUpdateRequest creditCardUpdateRequest );
    Customer updateCustomerCreditCard (CreditCardUpdateRequest creditCardUpdateRequest );

}
