package com.id.fitnessBni.service;

import com.id.fitnessBni.exception.NotFoundException;
import com.id.fitnessBni.dto.trainingpackage.TrainingPackakgeCreateRequest;
import com.id.fitnessBni.dto.trainingpackage.TrainingPackakgeUpdateRequest;
import com.id.fitnessBni.entity.Training;
import com.id.fitnessBni.entity.TrainingPackage;
import com.id.fitnessBni.repository.TrainingPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class TrainingPackageService {

    @Autowired
    private TrainingPackageRepository trainingPackageRepository;

    public List<TrainingPackage> findAll() {

        List<TrainingPackage> trainingPackageList= trainingPackageRepository.findAll();
        return trainingPackageList;

    }

    public ResponseEntity<Object> findById(int id){
        TrainingPackage trainingPackage= trainingPackageRepository.findById(id);
        return new ResponseEntity<Object>(trainingPackage, HttpStatus.OK);
    }

    public ResponseEntity<Object> create(TrainingPackakgeCreateRequest trainingPackakgeCreateRequest) {
        Training newTraining = new Training();

        return new ResponseEntity<Object>(newTraining, HttpStatus.OK);
    }

    public ResponseEntity<Object> update(TrainingPackakgeUpdateRequest trainingPackakgeUpdateRequest) {

        Optional<TrainingPackage> newTrainingPackage = trainingPackageRepository.findById(trainingPackakgeUpdateRequest.getId());
        if (newTrainingPackage == null) throw new NotFoundException("data-tidak-ditemukan");

        return new ResponseEntity<Object>(newTrainingPackage, HttpStatus.OK);
    }

    public ResponseEntity<Object> delete(int id) {
        TrainingPackage newTrainingPackage = trainingPackageRepository.findById(id);
        if(newTrainingPackage == null) throw new NotFoundException("data-tidak-ditemukan");
        trainingPackageRepository.delete(newTrainingPackage);
        return new ResponseEntity<Object>(newTrainingPackage, HttpStatus.OK);
    }
}
