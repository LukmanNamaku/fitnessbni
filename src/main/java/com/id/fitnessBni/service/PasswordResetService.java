package com.id.fitnessBni.service;

import com.id.fitnessBni.exception.FoundException;
import com.id.fitnessBni.dto.customer.ForgotPassword;
import com.id.fitnessBni.dto.registrasi.EmailOtp;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class PasswordResetService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private JavaMailSender javaMailSender;

    public void requestPasswordReset(String email) {
        Optional<Customer> customerOptional = customerRepository.findByEmail(email);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            String resetToken = generateResetToken();
            customer.setResetToken(resetToken);
            customerRepository.save(customer);
            sendPasswordResetEmail(customer, resetToken);
        }

    }

    private String generateResetToken() {
        return UUID.randomUUID().toString();
    }

    private void sendPasswordResetEmail(Customer customer, String resetToken) {

        String subject = "Reset Password";
        String text = "Anda telah meminta reset password. Silakan klik link berikut untuk mereset kata sandi Anda: [LINK RESET]";
        String resetLink = "https://fitness.com/reset-password?token=" + resetToken;
        text = text.replace("[LINK RESET]", resetLink);

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(customer.getEmail());
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(text);

        try {
            javaMailSender.send(simpleMailMessage);
        }  catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

    public Customer updatePasswordCustomer(ForgotPassword forgotPassword) {

        Customer customer = customerRepository.findByAccountId(forgotPassword.getAccountId());
        String password = forgotPassword.getPassword();
        String repassword = forgotPassword.getRepassword();
        String otpGenerate = Integer.toString(registrationService.generateOTP());

        if (password.equals(repassword)) {
            customer.setPassword(forgotPassword.getPassword());
            customer.setVerificationCode(otpGenerate);
            customer.setVerified(false);

            EmailOtp newEmailOtp = new EmailOtp();
            newEmailOtp.setSubjectMail("Verify Email For Customer");
            newEmailOtp.setName(customer.getName());
            newEmailOtp.setEmail(customer.getEmail());
            newEmailOtp.setOtp(otpGenerate);
            newEmailOtp.setMessage("You Change Password, please input OTP : ");

            try {
                registrationService.sendOTPEmail(newEmailOtp);
            } catch (Exception e) {
                logger.error("Email not Sending, please check configuration", e);
            }

            customerRepository.save(customer);

        } else {
            throw new FoundException("password-tidak-sesuai");
        }

        return customer;
    }
}
