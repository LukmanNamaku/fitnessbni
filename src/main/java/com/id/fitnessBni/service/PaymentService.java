package com.id.fitnessBni.service;

import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.entity.PaymentStatus;
import com.id.fitnessBni.repository.CustomerRepository;
import com.id.fitnessBni.repository.PaymentStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class PaymentService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public boolean verifyPaymentOTP(String email, String otp) {

        PaymentStatus paymentStatus = paymentStatusRepository.findByStatusCode("L");

        Optional<Customer> customerOptional = customerRepository.findByEmail(email);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();

            if (customer.getPaymentOTP() != null &&
                    customer.getPaymentOTP().equals(otp) &&
                    LocalDateTime.now().isBefore(customer.getPaymentOTPExpiration())) {

                customer.setPaymentStatus(paymentStatus);
                customer.setPaymentOTP(null);
                customer.setPaymentOTPExpiration(null);


                customerRepository.save(customer);
                return true;
            }
        }

        return false;

    }

    public boolean verifyBillAmount(int accuntId, BigDecimal expectedBillAmount) {

        Optional<Customer> customerOptional= customerRepository.findById(accuntId);

        if (customerOptional.isPresent()) {
            Customer customer= customerOptional.get();

            if (customer.getBillAmount() != null && customer.getBillAmount().compareTo(expectedBillAmount) == 0) {
                customer.setBillVerified(true);
                customerRepository.save(customer);
                return true;
            }
        }

        return false;

    }

    public boolean isPaymentOTPExpired(String email) {

        Optional<Customer> participantOptional = customerRepository.findByEmail(email);

        if (participantOptional.isPresent()) {
            Customer customer = participantOptional.get();
            if (customer.getPaymentOTPExpiration() != null &&
                    LocalDateTime.now().isAfter(customer.getPaymentOTPExpiration())) {
                return true;
            }
        }

        return false;

    }

    private String generateVerificationCode() {
        int codeLength = 6;
        String allowedChars = "0123456789";

        Random random = new Random();
        StringBuilder verificationCode = new StringBuilder(codeLength);

        for (int i = 0; i < codeLength; i++) {
            int randomIndex = random.nextInt(allowedChars.length());
            char randomChar = allowedChars.charAt(randomIndex);
            verificationCode.append(randomChar);
        }

        return verificationCode.toString();
    }

    public void sendOTPEmailVerification(String email, int accountId) throws MessagingException {

        String code = generateVerificationCode();
        Optional<Customer> customerOptional = customerRepository.findById(accountId);

        Customer customer = new Customer();

        if (customerOptional.isPresent()) {
            customer = customerOptional.get();
            customer.setPaymentOTP(code);
            customerRepository.save(customer);
        }

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(customer.getEmail());
        simpleMailMessage.setSubject("Kode OTP Payment");
        simpleMailMessage.setText("Kode OTP anda adalah : " + code);

        try {
            javaMailSender.send(simpleMailMessage);
        }  catch (org.springframework.messaging.MessagingException e) {
            throw new RuntimeException(e);
        }

    }

}
