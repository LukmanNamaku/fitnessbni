package com.id.fitnessBni.service;

import com.id.fitnessBni.entity.Authentication;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.repository.AuthRepository;
import com.id.fitnessBni.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class AuthenticationService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Authentication login(String email, String password) {

        Optional<Customer> customerOptional = customerRepository.findByEmail(email);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            if (passwordEncoder.matches(password, customer.getPassword())) {
                Authentication authenticationToken = generateAuthToken(customer);
                authRepository.save(authenticationToken);
                return authenticationToken;
            }
        }
        return null;

    }

    public void logout(String token) {
        authRepository.deleteByToken(token);
    }

    public Authentication refreshToken(String token) {
        Authentication existingToken = authRepository.findByToken(token);
        if (existingToken != null) {
            existingToken.updateExpiration();
            authRepository.save(existingToken);
            return existingToken;
        }
        return null;
    }

    private Authentication generateAuthToken(Customer customer) {
        String token = UUID.randomUUID().toString();
        LocalDateTime expirationDateTime = LocalDateTime.now().plusHours(1);
        return new Authentication(token, expirationDateTime, customer);
    }
}
