package com.id.fitnessBni.service;

import com.id.fitnessBni.dto.membershipstatus.MembershipStatusCreateRequest;
import com.id.fitnessBni.dto.membershipstatus.MembershipStatusUpdateRequest;
import com.id.fitnessBni.entity.MembershipStatus;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */


public interface MembershipStatusService {

    MembershipStatus findById(int id);
    List<MembershipStatus> findAll();
    MembershipStatus save(MembershipStatusCreateRequest membershipStatusCreateRequest);
    MembershipStatus update(MembershipStatusUpdateRequest membershipStatusUpdateRequest);
    MembershipStatus delete(int id);

}
