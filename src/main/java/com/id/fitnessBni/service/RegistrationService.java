package com.id.fitnessBni.service;

import com.id.fitnessBni.exception.FoundException;
import com.id.fitnessBni.dto.registrasi.EmailOtp;
import com.id.fitnessBni.dto.registrasi.RegistrasiRequest;
import com.id.fitnessBni.dto.registrasi.ValidasiRegistrasiRequest;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.entity.MembershipStatus;
import com.id.fitnessBni.entity.PaymentStatus;
import com.id.fitnessBni.repository.CustomerRepository;
import com.id.fitnessBni.repository.MembershipStatusRepository;
import com.id.fitnessBni.repository.PaymentStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Service
public class RegistrationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private MembershipStatusRepository membershipStatusRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public Customer saveRegistrasion(RegistrasiRequest registrasiRequest){

        PaymentStatus paymentStatus = paymentStatusRepository.findByStatusCode("B");

        MembershipStatus membershipStatus = membershipStatusRepository.findByStatusMemberCode("N");

        Customer customerEmail = customerRepository.findTopByEmail(registrasiRequest.getEmail());
        if(customerEmail != null){
            throw new FoundException("email-sudah-terdaftar");
        }

        String otpGenerate = Integer.toString(generateOTP());

        Customer newCustomer = new Customer();
        newCustomer.setName(registrasiRequest.getName());
        newCustomer.setEmail(registrasiRequest.getEmail());
        newCustomer.setPassword(registrasiRequest.getPassword());
        newCustomer.setPhoneNumber(registrasiRequest.getPhoneNumber());

        newCustomer.setPaymentStatus(paymentStatus);
        newCustomer.setMembershipStatus(membershipStatus);

        newCustomer.setVerificationCode(otpGenerate);

        EmailOtp newEmailOtp = new EmailOtp();
        newEmailOtp.setSubjectMail("Verify Email For Customer");
        newEmailOtp.setName(newCustomer.getName());
        newEmailOtp.setEmail(newCustomer.getEmail());
        newEmailOtp.setOtp(otpGenerate);
        newEmailOtp.setMessage("Thank you For the Registration, Insert This Code Verivication For Visit Member ");

        try {
            sendOTPEmail(newEmailOtp);
        } catch (Exception e){
            logger.error("Email not Sending, please check configuration", e);
        }

        customerRepository.save(newCustomer);

        return newCustomer;
    }

    public Customer validasiRegistrasion(ValidasiRegistrasiRequest validasiRegistrasiRequest){
        Customer customerValidasi = customerRepository.findByEmailAndVerificationCode(validasiRegistrasiRequest.getEmail(), validasiRegistrasiRequest.getVerificationCode());
        if(customerValidasi == null){
            throw new FoundException("user-tidak-terdaftar");
        }
        customerValidasi.setVerified(true);
        customerValidasi.setVerificationCode(null);
        customerRepository.save(customerValidasi);
        return customerValidasi;

    }

    public int generateOTP(){
        Random random = new Random();
        Integer otp = 100000 + random.nextInt(900000);
//        RandomString.make(6);
        return otp;
    }

    public void sendOTPEmail(EmailOtp emailOtp){

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(emailOtp.getEmail());
        simpleMailMessage.setSubject(emailOtp.getSubjectMail());
        simpleMailMessage.setText(emailOtp.getMessage() + emailOtp.getOtp());

        logger.info(emailOtp.getEmail());
        logger.info(emailOtp.getSubjectMail());
        logger.info(emailOtp.getOtp());

        javaMailSender.send(simpleMailMessage);

    }

}
