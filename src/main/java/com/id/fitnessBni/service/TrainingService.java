package com.id.fitnessBni.service;

import com.id.fitnessBni.exception.NotFoundException;
import com.id.fitnessBni.dto.trainingpackage.TrainingRequest;
import com.id.fitnessBni.entity.Training;
import com.id.fitnessBni.repository.TrainingRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class TrainingService {

    @Autowired
    private TrainingRepository trainingRepository;

    public List<Training> findAll() {

        List<Training> trainingList= trainingRepository.findAll();
        return trainingList;

    }

    public ResponseEntity<Object> findById(int id){

        Training training= trainingRepository.findById(id);
        return new ResponseEntity<Object>(training, HttpStatus.OK);

    }

    public ResponseEntity<Object> create(TrainingRequest trainingRequest) {

        Training newTraining = new Training();
        newTraining.setName(trainingRequest.getName());
        newTraining.setDescription(trainingRequest.getDescription());
        newTraining.setDurationInMinutes(trainingRequest.getDurationInMinutes());
        trainingRepository.save(newTraining);
        return new ResponseEntity<Object>(newTraining, HttpStatus.OK);

    }

    public ResponseEntity<Object> update(TrainingRequest trainingRequest) {

        Training newTraining = trainingRepository.findById(trainingRequest.getId());
        if (newTraining == null) throw new NotFoundException("data-tidak-ditemukan");
        BeanUtils.copyProperties(trainingRequest, newTraining);
        trainingRepository.save(newTraining);

        return new ResponseEntity<Object>(newTraining, HttpStatus.OK);

    }

    public ResponseEntity<Object> delete(int id) {

        Training newTraining = trainingRepository.findById(id);
        if(newTraining == null) throw new NotFoundException("data-tidak-ditemukan");
        trainingRepository.delete(newTraining);
        return new ResponseEntity<Object>(newTraining, HttpStatus.OK);

    }
}