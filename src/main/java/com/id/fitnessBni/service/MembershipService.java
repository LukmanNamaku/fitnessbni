package com.id.fitnessBni.service;

import com.id.fitnessBni.entity.MemberPackage;
import com.id.fitnessBni.repository.MembershipRepository;
import com.id.fitnessBni.repository.TrainingPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Service
public class MembershipService {

    @Autowired
    private MembershipRepository membershipRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TrainingPackageRepository trainingPackageRepository;

    public MemberPackage membershipToService(int accountId, int serviceMenuId) {
        return null;
    }

    public void cancelMembership(int membershipId) {
        MemberPackage memberPackage = membershipRepository.findById(membershipId).get();
        membershipRepository.delete(memberPackage);
    }

    public void extendMembership(int membershipId, int numberOfSessions) {
        MemberPackage memberPackage = membershipRepository.findById(membershipId).get();
        memberPackage.setEndDate(memberPackage.getEndDate().plusDays(numberOfSessions));
        memberPackage.setRemainingSessions(memberPackage.getRemainingSessions() + numberOfSessions);
        membershipRepository.save(memberPackage);
    }
}
