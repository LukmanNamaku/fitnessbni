package com.id.fitnessBni.service.impl;

import com.id.fitnessBni.exception.NotFoundException;
import com.id.fitnessBni.dto.membershipstatus.MembershipStatusCreateRequest;
import com.id.fitnessBni.dto.membershipstatus.MembershipStatusUpdateRequest;
import com.id.fitnessBni.entity.MembershipStatus;
import com.id.fitnessBni.repository.MembershipStatusRepository;
import com.id.fitnessBni.service.MembershipStatusService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Service
public class MembershipStatusServiceImpl  implements MembershipStatusService {

    @Autowired
    private MembershipStatusRepository membershipStatusRepository;

//    @Autowired
//    private BCryptPasswordEncoder bcryptEncoder;

    @Override
    public List<MembershipStatus> findAll() {
        List<MembershipStatus> newMembershipStatus = membershipStatusRepository.findAll();
        return newMembershipStatus;
    }

    @Override
    public MembershipStatus findById(int id) {

        MembershipStatus newMembershipStatus = membershipStatusRepository.findById(id);
        if (newMembershipStatus == null){
            throw new NotFoundException("Membership-status-tidak-ditemukan");
        }
        return newMembershipStatus;
    }

    @Override
    public MembershipStatus save(MembershipStatusCreateRequest membershipStatusCreateRequest) {
        MembershipStatus newMembershipStatus = new MembershipStatus();
        newMembershipStatus.setStatusMemberCode(membershipStatusCreateRequest.getStatusMemberCode());
        newMembershipStatus.setStatusMemberDesc(membershipStatusCreateRequest.getStatusMemberDesc());
        return membershipStatusRepository.save(newMembershipStatus);
    }

    @Override
    public MembershipStatus update(MembershipStatusUpdateRequest membershipStatusUpdateRequest) {

        MembershipStatus newMembershipStatus = membershipStatusRepository.findById(membershipStatusUpdateRequest.getId());
        if (newMembershipStatus == null){
            throw new NotFoundException("memberpackage-status-tidak-ditemukan");
        }
        BeanUtils.copyProperties(membershipStatusUpdateRequest, newMembershipStatus);
        return membershipStatusRepository.save(newMembershipStatus);

    }

    @Override
    public MembershipStatus delete(int id) {
        MembershipStatus newMembershipStatus = membershipStatusRepository.deleteById(id);
        return newMembershipStatus;
    }
}
