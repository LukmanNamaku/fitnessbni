package com.id.fitnessBni.service.impl;

import com.id.fitnessBni.exception.NotFoundException;
import com.id.fitnessBni.dto.creditcard.CreditCardUpdateRequest;
import com.id.fitnessBni.dto.customer.CustomerCreateRequest;
import com.id.fitnessBni.dto.customer.CustomerUpdateRequest;
import com.id.fitnessBni.entity.CreditCard;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.repository.CreditCardRepositoy;
import com.id.fitnessBni.repository.CustomerRepository;
import com.id.fitnessBni.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Service
public class CustomerServiceImpl  implements CustomerService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CreditCardRepositoy creditCardRepositoy;

    @Override
    public Customer save(CustomerCreateRequest customerCreateRequest) {
        Customer newCustomer = new Customer();
        BeanUtils.copyProperties(customerCreateRequest, newCustomer);
        return customerRepository.save(newCustomer);
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> newCustomer = customerRepository.findAll();
        return newCustomer;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Customer findOne(String username) {
        return null;
    }

    @Override
    public Customer findById(int id) {
        return null;
    }

    @Override
    public Customer update(CustomerUpdateRequest customerUpdateRequest) {

        Customer newCustomer = customerRepository.findByAccountId(customerUpdateRequest.getAccountId());
        newCustomer.setAccountId(customerUpdateRequest.getAccountId());
        newCustomer.setName(customerUpdateRequest.getName());
        newCustomer.setEmail(customerUpdateRequest.getEmail());
        newCustomer.setPhoneNumber(customerUpdateRequest.getPhoneNumber());
        customerRepository.save(newCustomer);

        return newCustomer;
    }

    @Override
    public Customer addCustomerCreditCard(CreditCardUpdateRequest creditCardUpdateRequest) {

        Customer customer = customerRepository.findByAccountId(creditCardUpdateRequest.getAccountId());

        CreditCard newCreditCard = new CreditCard();
        newCreditCard.setCardName(creditCardUpdateRequest.getCardName());
        newCreditCard.setCardNumber(creditCardUpdateRequest.getCardNumber());
        newCreditCard.setCcvCode(creditCardUpdateRequest.getCcvCode());
        newCreditCard.setExpirationDate(creditCardUpdateRequest.getExpirationDate());
        creditCardRepositoy.save(newCreditCard);

        customer.setCreditCardInfo(newCreditCard);
        customerRepository.save(customer);

        return customer;

    }

    @Override
    public Customer updateCustomerCreditCard(CreditCardUpdateRequest creditCardUpdateRequest) {

        Customer newCustomer = customerRepository.findByAccountId(creditCardUpdateRequest.getAccountId());
        CreditCard newCreditCard = creditCardRepositoy.findById(newCustomer.getCreditCardInfo().getCreditCardId());
        if (newCreditCard == null) {
            throw new NotFoundException("kartu-kredit-tidak-ditemukan");
        }
        newCreditCard.setCreditCardId(newCustomer.getCreditCardInfo().getCreditCardId());
        newCreditCard.setCardName(creditCardUpdateRequest.getCardName());
        newCreditCard.setCardNumber(creditCardUpdateRequest.getCardNumber());
        newCreditCard.setCcvCode(creditCardUpdateRequest.getCcvCode());
        newCreditCard.setExpirationDate(creditCardUpdateRequest.getExpirationDate());
        creditCardRepositoy.save(newCreditCard);
        return newCustomer;
    }


}
