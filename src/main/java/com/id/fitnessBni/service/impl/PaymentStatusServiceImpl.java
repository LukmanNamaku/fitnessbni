package com.id.fitnessBni.service.impl;

import com.id.fitnessBni.exception.NotFoundException;
import com.id.fitnessBni.dto.paymentstatus.PaymentStatusCreateRequest;
import com.id.fitnessBni.dto.paymentstatus.PaymentStatusUpdateRequest;
import com.id.fitnessBni.entity.PaymentStatus;
import com.id.fitnessBni.repository.PaymentStatusRepository;
import com.id.fitnessBni.service.PaymentStatusService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */


@Service
public class PaymentStatusServiceImpl implements PaymentStatusService {

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

//    @Autowired
//    private BCryptPasswordEncoder bcryptEncoder;

    @Override
    public List<PaymentStatus> findAll() {
        List<PaymentStatus> newPaymentStatus = paymentStatusRepository.findAll();
        return newPaymentStatus;
    }

    @Override
    public PaymentStatus save(PaymentStatusCreateRequest paymentStatusCreateRequest) {
        PaymentStatus newPaymentStatus = new PaymentStatus();
        newPaymentStatus.setStatusCode(paymentStatusCreateRequest.getStatusCode());
        newPaymentStatus.setStatusDesc(paymentStatusCreateRequest.getStatusDesc());
        return paymentStatusRepository.save(newPaymentStatus);

    }

    @Override
    public PaymentStatus update(PaymentStatusUpdateRequest paymentStatusUpdateRequest) {

        PaymentStatus newPaymentStatus = paymentStatusRepository.findById(paymentStatusUpdateRequest.getId());
        if (newPaymentStatus == null){
            throw new NotFoundException("payment-status-tidak-ditemukan");
        }
        BeanUtils.copyProperties(paymentStatusUpdateRequest, newPaymentStatus);
        paymentStatusRepository.save(newPaymentStatus);
        return newPaymentStatus;

    }

    @Override
    public PaymentStatus findById(int id) {
        PaymentStatus newPaymentStatus = paymentStatusRepository.findById(id);
        if (newPaymentStatus == null){
            throw new NotFoundException("payment-status-tidak-ditemukan");
        }
        return newPaymentStatus;

    }

    @Override
    public PaymentStatus delete(int id) {
        PaymentStatus newPaymentStatus = paymentStatusRepository.deleteById(id);
        return newPaymentStatus;

    }
}
