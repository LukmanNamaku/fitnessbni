package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.membershipstatus.MembershipStatusCreateRequest;
import com.id.fitnessBni.dto.membershipstatus.MembershipStatusUpdateRequest;
import com.id.fitnessBni.entity.MembershipStatus;
import com.id.fitnessBni.service.MembershipStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@RestController
@RequestMapping("/api/managedmembership")
public class MembershipStatusController {

    @Autowired
    private MembershipStatusService membershipStatusService;

    @PostMapping
    ResponseEntity<MembershipStatus> saveNewStatusMembership(@RequestBody MembershipStatusCreateRequest membershipStatusCreateRequest){
        return ResponseEntity.ok(membershipStatusService.save(membershipStatusCreateRequest));
    }

    @GetMapping
    ResponseEntity<List<MembershipStatus>> listStatusMembership(){
        return ResponseEntity.ok(membershipStatusService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<MembershipStatus> getOneStatusMembership(@PathVariable int id){
        return ResponseEntity.ok(membershipStatusService.findById(id));
    }

    @PatchMapping
    ResponseEntity<MembershipStatus> updateStatusMembership(@RequestBody MembershipStatusUpdateRequest membershipStatusUpdateRequest) {
        return ResponseEntity.ok(membershipStatusService.update(membershipStatusUpdateRequest));
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteStatusMembership(@PathVariable int id) {
        return ResponseEntity.ok(membershipStatusService.delete(id));
    }

}