package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.trainingpackage.TrainingPackakgeCreateRequest;
import com.id.fitnessBni.dto.trainingpackage.TrainingPackakgeUpdateRequest;
import com.id.fitnessBni.service.TrainingPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@RestController
@RequestMapping("/api/managed-training-package")
public class TrainingPackageController {

    @Autowired
    private TrainingPackageService trainingPackageService;

    @GetMapping
    ResponseEntity listTrainingPackage(){
        return ResponseEntity.ok(trainingPackageService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity getDetailTrainingPackage(@PathVariable int id){ return ResponseEntity.ok(trainingPackageService.findById(id)); }

    @PostMapping
    ResponseEntity <Object> addNewTrainingPackage(@RequestBody TrainingPackakgeCreateRequest trainingPackakgeCreateRequest){
        return ResponseEntity.ok(trainingPackageService.create(trainingPackakgeCreateRequest));
    }

    @PatchMapping
    ResponseEntity <Object> updateTrainingPackage(@RequestBody TrainingPackakgeUpdateRequest trainingPackakgeUpdateRequest) {
        return ResponseEntity.ok(trainingPackageService.update(trainingPackakgeUpdateRequest));
    }

    @DeleteMapping("/{id}")
    ResponseEntity <Object> deleteTraingPackage(@PathVariable int id){
        return ResponseEntity.ok(trainingPackageService.delete(id));
    }
}
