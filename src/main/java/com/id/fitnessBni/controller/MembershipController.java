package com.id.fitnessBni.controller;

import com.id.fitnessBni.service.MembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 08/11/2023
 */

@RestController
@RequestMapping("/membership")
public class MembershipController {

    @Autowired
    private MembershipService membershipService;

    @PostMapping("/member")
    ResponseEntity<Object> membershipToService(@RequestParam int accountId, @RequestParam int trainingPackageId) {
        membershipService.membershipToService(accountId, trainingPackageId);
        return ResponseEntity.ok("Membership Berhasil.");
    }

    @PostMapping("/cancel")
    ResponseEntity<Object> cancelMembership(@RequestParam int trainingPackageId) {
        membershipService.cancelMembership(trainingPackageId);
        return ResponseEntity.ok("Membershipberhasil dibatalkan.");
    }

    @PostMapping("/extend")
    ResponseEntity<Object> extendMembership(@RequestParam int subscriptionId, @RequestParam int numberOfSessions) {
        membershipService.extendMembership(subscriptionId, numberOfSessions);
        return ResponseEntity.ok("Extends berhasil");
    }
}
