package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.registrasi.RegistrasiRequest;
import com.id.fitnessBni.dto.registrasi.ValidasiRegistrasiRequest;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.service.CustomerService;
import com.id.fitnessBni.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@RestController
@RequestMapping("/api/registration")
public class RegistrationController{

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private CustomerService customerService;

    @GetMapping
    ResponseEntity<List<Customer>> listCustomer(){
        return ResponseEntity.ok(customerService.findAll());
    }

    @PostMapping
    ResponseEntity<Customer> registrasi(@RequestBody RegistrasiRequest registrasiRequest){
        return ResponseEntity.ok(registrationService.saveRegistrasion(registrasiRequest));
    }

    @PostMapping("/verification")
    ResponseEntity<Customer> verificationRegistration(@RequestBody ValidasiRegistrasiRequest validasiRegistrasiRequest){
        return ResponseEntity.ok(registrationService.validasiRegistrasion(validasiRegistrasiRequest));
    }

}
