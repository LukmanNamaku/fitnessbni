package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.trainingpackage.TrainingRequest;
import com.id.fitnessBni.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@RestController
@RequestMapping("/api/managed-training")
public class TrainingController {

    @Autowired
    private TrainingService trainingService;

    @GetMapping
    ResponseEntity listTraining(){
        return ResponseEntity.ok(trainingService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity getDetailTraining(@PathVariable int id){
        return ResponseEntity.ok(trainingService.findById(id));
    }

    @PostMapping
    ResponseEntity <Object> addNewTraining(@RequestBody TrainingRequest trainingRequest){
        return ResponseEntity.ok(trainingService.create(trainingRequest));
    }

    @PatchMapping
    ResponseEntity <Object> updateTraining(@RequestBody TrainingRequest trainingRequest) {
        return ResponseEntity.ok(trainingService.update(trainingRequest));
    }

    @DeleteMapping("/{id}")
    ResponseEntity <Object> deleteTraing(@PathVariable int id) {
        return ResponseEntity.ok(trainingService.delete(id));
    }

}
