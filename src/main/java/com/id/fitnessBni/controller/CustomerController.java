package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.creditcard.CreditCardUpdateRequest;
import com.id.fitnessBni.dto.customer.CustomerCreateRequest;
import com.id.fitnessBni.dto.customer.CustomerUpdateRequest;
import com.id.fitnessBni.dto.customer.ForgotPassword;
import com.id.fitnessBni.entity.CreditCard;
import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.service.CustomerService;
import com.id.fitnessBni.service.PasswordResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PasswordResetService passwordResetService;

    @GetMapping
    ResponseEntity<List<Customer>> listCustomer(){
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<Customer> getOneCustomer(@PathVariable int id){
        return ResponseEntity.ok(customerService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Customer> saveCustomer(@RequestBody CustomerCreateRequest customerCreateRequest){
        return ResponseEntity.ok(customerService.save(customerCreateRequest));
    }

    @PatchMapping("/update-detail-customer")
    ResponseEntity updateCustomer(@RequestBody CustomerUpdateRequest customerUpdateRequest) {
        customerService.update(customerUpdateRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteCustomer(@PathVariable int id) {
        customerService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/credit-card-add")
    ResponseEntity addCustomerCreditCard(@RequestBody CreditCardUpdateRequest creditCardUpdateRequest) {
        customerService.addCustomerCreditCard(creditCardUpdateRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/forgot-password")
    ResponseEntity<Customer> updatePassword(@RequestBody ForgotPassword forgotPassword) {
        return ResponseEntity.ok(passwordResetService.updatePasswordCustomer(forgotPassword));
    }

}
