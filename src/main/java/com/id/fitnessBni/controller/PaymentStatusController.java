package com.id.fitnessBni.controller;

import com.id.fitnessBni.dto.paymentstatus.PaymentStatusCreateRequest;
import com.id.fitnessBni.dto.paymentstatus.PaymentStatusUpdateRequest;
import com.id.fitnessBni.entity.PaymentStatus;
import com.id.fitnessBni.service.PaymentStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@RestController
@RequestMapping("/api/managedpayment")
public class PaymentStatusController {

    @Autowired
    private PaymentStatusService paymentStatusService;

    @PostMapping
    ResponseEntity<PaymentStatus> saveNewStatusPayment(@RequestBody PaymentStatusCreateRequest paymentStatusCreateRequest){
        return ResponseEntity.ok(paymentStatusService.save(paymentStatusCreateRequest));
    }

    @GetMapping
    ResponseEntity<List<PaymentStatus>> listStatusPayment(){
        return ResponseEntity.ok(paymentStatusService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<PaymentStatus> getOneStatusPayment(@PathVariable int id){
        return ResponseEntity.ok(paymentStatusService.findById(id));
    }

    @PatchMapping
    ResponseEntity updateStatusPayment(@RequestBody PaymentStatusUpdateRequest paymentStatusUpdateRequest) {
        paymentStatusService.update(paymentStatusUpdateRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteStatusPayment(@PathVariable int id) {
        paymentStatusService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
