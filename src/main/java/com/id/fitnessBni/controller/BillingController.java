package com.id.fitnessBni.controller;

import com.id.fitnessBni.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 08/11/2023
 */

@RestController
@RequestMapping("/billing")
public class BillingController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/verify-bill")
    public ResponseEntity<String> verifyBillAmount(@RequestParam int accounId, @RequestParam BigDecimal expectedBillAmount) {
        if (paymentService.verifyBillAmount(accounId,expectedBillAmount)) {
            return ResponseEntity.ok("Verifikasi tagihan berhasil.");
        } else {
            return ResponseEntity.badRequest().body("Verifikasi tagihan gagal.");
        }
    }

}
