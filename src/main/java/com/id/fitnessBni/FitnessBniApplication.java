package com.id.fitnessBni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessBniApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitnessBniApplication.class, args);
	}

}
