package com.id.fitnessBni.dto.paymentstatus;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.paymentstatus
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Setter
@Getter
public class PaymentStatusUpdateRequest {

    private int Id;
    private String StatusCode;
    private String StatusDesc;

}
