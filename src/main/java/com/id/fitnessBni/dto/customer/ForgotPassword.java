package com.id.fitnessBni.dto.customer;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.customer
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */


@Data
@Getter
@Setter
public class ForgotPassword {
    private Integer accountId;
    private String password;
    private String repassword;
}
