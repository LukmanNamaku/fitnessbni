package com.id.fitnessBni.dto.customer;

import com.id.fitnessBni.entity.CreditCard;
import com.id.fitnessBni.entity.MembershipStatus;
import com.id.fitnessBni.entity.PaymentStatus;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.customer
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Getter
@Setter
public class CustomerDTO {

    private String name;
    private String email;
    private String password;
    private String phoneNumber;
    private boolean isVerified;
    private String verificationCode;
    private String resetToken;

    private PaymentStatus paymentStatus;
    private String paymentOTP;
    private LocalDateTime paymentOTPExpiration;
    private BigDecimal billAmount;
    private boolean isBillVerified;

    private CreditCard creditCardInfo;

    private MembershipStatus membershipStatus;

}
