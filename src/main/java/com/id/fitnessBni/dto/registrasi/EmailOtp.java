package com.id.fitnessBni.dto.registrasi;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.registrasi
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Getter
@Setter
public class EmailOtp {

    private String subjectMail;
    private String name;
    private String email;
    private String otp;
    private String message;

}
