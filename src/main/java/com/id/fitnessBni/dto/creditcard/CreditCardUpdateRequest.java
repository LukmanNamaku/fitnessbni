package com.id.fitnessBni.dto.creditcard;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.creditcard
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Data
@Setter
@Getter
public class CreditCardUpdateRequest {

    private int accountId;
    private String cardName;
    private String cardNumber;
    private String ccvCode;
    private Date expirationDate;

}
