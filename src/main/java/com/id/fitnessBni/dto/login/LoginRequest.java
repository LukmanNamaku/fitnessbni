package com.id.fitnessBni.dto.login;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.login
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 08/11/2023
 */

@Data
@Getter
@Setter
public class LoginRequest {

    private String email;
    private String password;

}
