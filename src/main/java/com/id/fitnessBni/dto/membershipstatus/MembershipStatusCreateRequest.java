package com.id.fitnessBni.dto.membershipstatus;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.paymentstatus
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Setter
@Getter
public class MembershipStatusCreateRequest {

    private String StatusMemberCode;
    private String StatusMemberDesc;

}
