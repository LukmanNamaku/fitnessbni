package com.id.fitnessBni.dto.memberpackage;

import com.id.fitnessBni.entity.Customer;
import com.id.fitnessBni.entity.TrainingPackage;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.customer
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Getter
@Setter
public class MembershipPackageCreateRequest {

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private int remainingSessions;
    private Customer customer;
    private TrainingPackage trainingPackage;

}
