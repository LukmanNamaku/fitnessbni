package com.id.fitnessBni.dto.trainingpackage;

import com.id.fitnessBni.entity.Training;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto.customer
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@Data
@Getter
@Setter
public class TrainingPackakgeCreateRequest {

    private String name;
    private double pricePerSession;
    private String schedule;
    private int totalSessions;
    private int durationInMinutes;
    private List<Training> training;

}
