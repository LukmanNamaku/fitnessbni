package com.id.fitnessBni.dto.trainingpackage;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 07/11/2023
 */

@Data
@Setter
@Getter
public class TrainingRequest {

    private int id;
    private String name;
    private int durationInMinutes;
    private String description;

}
