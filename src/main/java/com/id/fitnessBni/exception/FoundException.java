package com.id.fitnessBni.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @Project fitnessBni
 * @Package com.id.fitnessBni.exception
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 06/11/2023
 */

@ResponseStatus(HttpStatus.FOUND)
public class FoundException extends RuntimeException{

    public FoundException(String message) {
        super(String.valueOf(message));
    }

    public FoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
